CFLAGS=--std=c17 -Isrc/ -ggdb
BUILDDIR=build
SRCDIR=src
CC=gcc
ASM=nasm
ASMFLAGS=-felf64 -g

all: $(BUILDDIR)/main.o $(BUILDDIR)/bmp_handler.o $(BUILDDIR)/util.o $(BUILDDIR)/sepia.o $(BUILDDIR)/sepia_filter.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)


$(BUILDDIR)/sepia.o: $(SRCDIR)/sepia.asm build
	$(ASM) $(ASMFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/bmp_handler.o: $(SRCDIR)/bmp_handler.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia_filter.o: $(SRCDIR)/sepia_filter.c build $(BUILDDIR)/sepia.o
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)