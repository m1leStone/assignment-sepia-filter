#ifndef MY_PROJECT_UTIL_H
#define MY_PROJECT_UTIL_H

#include <stdbool.h>

bool measure_execution_time_c(const char* source_path, const char* destination_path);

bool measure_execution_time_asm(const char* source_path, const char* destination_path);

#endif
