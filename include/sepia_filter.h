#ifndef SEPIA_SEPIA_FILTER_H
#define SEPIA_SEPIA_FILTER_H

#include "image.h"

void sepia_filter_c(struct image);

void sepia_filter_asm(struct image);

#endif
