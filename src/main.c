#include "../include/main.h"
#include "../include/util.h"
#include <stdio.h>


int main(int argc, char** argv) {

    if (argc != 3 && argc != 1){
        fprintf(stderr, "Wrong number of arguments.\n Expected 0 or 2 Example of usage: %s <source-image> <transformed-image>\n" , argv[0]);
        return 1;
    }

    const char* source = "img.bmp";
    const char* destination = "post_filter.bmp";

    if (argc == 3){
        source = argv[1];
        destination = argv[2];
    }

    if (!measure_execution_time_asm(source, destination)
     || !measure_execution_time_c(source, destination) ){
        fprintf(stderr, "Failed to apply_filter to image\n");
        return 1;
    }

    return 0;
}
