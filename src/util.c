#include "../include/util.h"
#include "../include/bmp_handler.h"
#include "../include/sepia_filter.h"
#include <malloc.h>
#include <stdio.h>
#include <sys/resource.h>
#include <stdlib.h>

#define NUMBER_OF_FUNCTION_CALLS 20


static struct image copy_image (struct image const original_image){
    struct image new_image = {.height = original_image.height,
            .width = original_image.width,
            .data = malloc(sizeof (struct pixel) * original_image.height * original_image.width)};
    for (size_t i = 0; i < original_image.height * original_image.width; i++)
        new_image.data[i] = original_image.data[i];

    return new_image;
}

static bool measure_execution_time_generic(const char* source_path, const char* destination_path, void(*func)(struct image)){
    struct image img;
    if (!bmp_to_image(source_path, &img)){
        fprintf(stderr, "unable to apply filter because some error has occurred\n");
        if (img.data != NULL) free(img.data);
        return false;
    }
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage( RUSAGE_SELF, &r );
    start = r.ru_utime;
    struct image filtered_once = copy_image(img);
    func(filtered_once);
    for (int i = 0; i < NUMBER_OF_FUNCTION_CALLS; i++) {
        func(img);
    }
    getrusage( RUSAGE_SELF, &r );
    end = r.ru_utime;
    long res = ( ( end.tv_sec - start.tv_sec ) * 1000L ) + end.tv_usec - start.tv_usec;
    printf( "%ld\n", res );

    if (!image_to_bmp(&filtered_once, destination_path)){
        fprintf(stderr, "unable to apply filter because some error has occurred\n");
        if (filtered_once.data != NULL) free(filtered_once.data);
        return false;
    }
    free(filtered_once.data);
    fprintf(stdout, "filter had been successfully applied %d times\n", NUMBER_OF_FUNCTION_CALLS);
    return true;

}

bool measure_execution_time_c(const char* source_path, const char* destination_path){
    fprintf(stdout, "Execution time x%d of the C implementation (in milliseconds): ", NUMBER_OF_FUNCTION_CALLS);
    return measure_execution_time_generic(source_path, destination_path, &sepia_filter_c);
}

bool measure_execution_time_asm(const char* source_path, const char* destination_path){
    fprintf(stdout, "Execution time x%d of the ASM implementation (in milliseconds): ", NUMBER_OF_FUNCTION_CALLS);
    return measure_execution_time_generic(source_path, destination_path, &sepia_filter_asm);
}


